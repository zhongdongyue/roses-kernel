package cn.stylefeng.guns.roses.email.config;

import cn.stylefeng.guns.roses.email.executor.MailExecutorImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 邮件服务的配置
 *
 * @author fengshuonan
 * @Date 2018/7/6 下午3:24
 */
@Configuration
public class ServiceAutoConfiguration {

    @Bean
    public MailExecutorImpl mailServiceProvider() {
        return new MailExecutorImpl();
    }

}