package cn.stylefeng.guns.roses.email.executor;


import cn.stylefeng.guns.roses.email.core.mail.MailManager;
import cn.stylefeng.guns.roses.email.core.model.SendMailParam;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * 短信通知接口
 *
 * @author stylefeng
 * @Date 2018/7/5 21:05
 */
public class MailExecutorImpl implements MailExecutor {

    private static Logger logger = LoggerFactory.getLogger(MailExecutorImpl.class);

    @Autowired
    private MailManager mailManager;

    @Override
    public void sendMail(SendMailParam sendMailParam) {
        logger.info("email调用入参：" + JSONObject.toJSON(sendMailParam).toString());
        mailManager.sendMail(sendMailParam);
    }

}
