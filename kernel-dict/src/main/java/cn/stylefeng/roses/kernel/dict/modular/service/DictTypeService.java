package cn.stylefeng.roses.kernel.dict.modular.service;

import cn.stylefeng.roses.kernel.dict.modular.entity.DictType;
import cn.stylefeng.roses.kernel.dict.modular.model.DictTypeInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 字典类型服务
 *
 * @author fengshuonan
 * @Date 2019/12/20 3:16 下午
 */
public interface DictTypeService extends IService<DictType> {

    /**
     * 获取字典类型列表
     *
     * @param page         分页参数
     * @param dictTypeInfo 查询条件封装
     * @author fengshuonan
     * @Date 2018/7/25 下午12:36
     */
    List<DictTypeInfo> getDictTypeList(Page page, DictTypeInfo dictTypeInfo);

    /**
     * 添加字典类型
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午1:43
     */
    void addDictType(DictType dictType);

    /**
     * 修改字典类型
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午2:28
     */
    void updateDictType(DictType dictType);

    /**
     * 删除字典类型
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午2:43
     */
    void deleteDictType(String dictTypeId);

    /**
     * 修改字典状态
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午2:43
     */
    void updateDictTypeStatus(String dictTypeId, Integer status);

    /**
     * code校验
     *
     * @author fengshuonan
     * @Date 2018年11月16日
     */
    boolean checkCode(String dictTypeCode, String dictTypeId);

}
